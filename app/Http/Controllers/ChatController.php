<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Messages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class ChatController extends Controller
{
    public function index()
    {
        $publicGroup = Group::where('id', 1)->with(['messages.user', 'users'])->first();
        $users = User::all();
        return Inertia::render('Chat/Index', ['publicGroup' => $publicGroup, 'users' => $users]);
    }

    public function show($userID)
    {
        $group = Auth::user()->groups()
            ->where('groups.id', '!=', 1)
            ->whereHas('users', function ($q) use ($userID){
            return $q->where('user_id', $userID);
        })->first();
        if(!$group) {
            $group = Group::create([
                'title' => Auth::user()->id.'-'.$userID
            ]);
            $group->users()->attach([$userID, Auth::id()]);
        }
        return response()->json($group->load('messages.user', 'users'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'body' => 'required',
            'group_id' => 'required',
            ]);
        $message = Auth::user()->messages()->create($data);
        $message->group()->update(['last_use' => now()]);
        return back();
    }
}
