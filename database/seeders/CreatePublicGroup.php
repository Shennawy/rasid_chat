<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;

class CreatePublicGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Group::count()) {
            Group::create([
                'title' => 'Public Group',
                'status' => true
            ]);
        }
    }
}
